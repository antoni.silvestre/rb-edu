#!/bin/bash
if ! [ -f /media/pi/kiwix/library.xml ]; then
    sudo notify-send "KIWIX" "external drive not connected"
    exit 1
fi
if ! systemctl status kiwix &>/dev/null; then
    sudo notify-send "KIWIX" "kiwix not started!!"
    exit 1
fi

/usr/bin/chromium-browser 127.0.0.1
